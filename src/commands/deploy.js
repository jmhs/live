const {join, resolve, relative} = require('path')
const {createReadStream} = require('fs')
const {read} = require('../options')
const fetch = require('node-fetch')
const FormData = require('form-data')
const {promisify} = require('util')
const directoryExists = require('directory-exists')
const recursiveReaddir = require('recursive-readdir')
const Listr = require('listr')
const jwt = require('jsonwebtoken')
const throttle = require('lodash.throttle')
const Observable = require('zen-observable')
const createError = require('http-errors')
const chalk = require('chalk')

module.exports.command = ['deploy']
module.exports.aliases = ['publish', 'serve', 'sync', 'upload']
module.exports.desc = 'Make files available on the service'
module.exports.builder = {
  config: {
    type: 'string',
    describe: 'Configuration file'
  },
  root: {
    type: 'string',
    describe: 'Directory to serve'
  }
}

module.exports.handler = async function handler (argv) {
  await new Listr([{
    title: 'Loading configuration',
    task: async (context) => {
      const {
        access_token: accessToken,
        api: {origin = 'https://http2.live:8888'} = {}
      } = await read()
      if (accessToken === undefined) {
        throw new Error('No authentication token found')
      }
      const {payload: {exp}} = jwt.decode(accessToken, {complete: true})
      if (exp * 1e3 < Date.now()) {
        throw new Error('Authentication token expired')
      }
      context.accessToken = accessToken
      context.origin = origin
    }
  }, {
    title: 'Bundling custom options',
    skip: () => {
      if (argv.config === undefined) {
        return 'Using defaults'
      }
    },
    task: async (context) => {
      const filepath = resolve(process.cwd(), argv.config)
      const options = require(filepath)
      context.options = options
    }
  }, {
    title: 'Indexing files',
    task: async (context) => {
      const cwd = process.cwd()
      const publicDirectory = join(cwd, 'public')
      const root = argv.root ? resolve(argv.root)
        : (await promisify(directoryExists)(publicDirectory)) ? publicDirectory
        : cwd
      context.files = await recursiveReaddir(root)
      if (context.files.length === 0) throw new Error('No files found')
      context.root = root
    }
  }, {
    title: 'Uploading',
    task: (context) => new Observable((observer) => {
      const {accessToken, root, files, options} = context

      const total = files.length
      let pending = total
      const showProgress = throttle(observer.next.bind(observer), 1000 / 10)

      async function onEnd () {
        this.removeListener('error', onError)
        pending -= 1
        if (pending === 0) {
          showProgress('🚀')
        } else {
          const progress = Math.ceil(100 * (total - pending) / total)
          const percentage = `${String(progress).padStart(2)}%`
          const filename = relative(root, this.path)
          showProgress(`${percentage} ${filename}`)
        }
      }

      function onError (error) {
        this.removeListener('end', onEnd)
        observer.error(error)
      }

      const form = new FormData()

      if (options !== undefined) {
        form.append('options', JSON.stringify(options), {
          contentType: 'application/json'
        })
      }

      for (const file of files) {
        const readable = createReadStream(file)
        readable.once('error', onError)
        readable.once('end', onEnd)
        const filepath = relative(root, file)
        form.append('directory', readable, {filepath})
      }

      const url = `${context.origin}/deploy`
      fetch(url, {
        method: 'POST',
        body: form,
        headers: {
          authorization: `Bearer ${accessToken}`
        }
      })
        .then(async (response) => {
          if (response.ok) {
            const {domain} = await response.json()
            context.domain = domain
            observer.complete()
          } else {
            throw new createError[response.status]()
          }
        })
        .catch((error) => {
          observer.error(error)
        })
    })
  }])
    .run()
    .then(({domain}) => {
      console.log('')
      console.log('Deployment successful. Now available at:')
      console.log('')
      console.log(`  ${chalk.green(`https://${domain}/`)}`)
      console.log('')
    })
    .catch(() => {})
}
