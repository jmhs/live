const {read, write} = require('../options')
const omit = require('lodash.omit')
const unset = require('lodash.unset')
const get = require('lodash.get')
const merge = require('lodash.merge')
const flatten = require('flat')

module.exports.command = ['config']
module.exports.aliases = ['configure', 'configuration', 'setting', 'settings', 'option', 'options']
module.exports.desc = 'Edit user settings'
module.exports.builder = {
}

module.exports.handler = async function handler (argv) {
  const options = await read()
  const blacklist = ['v', 'version', 'h', 'help', '_', '$0']
  const payload = omit(argv, blacklist)
  const flattened = flatten(payload)
  const subcommand = argv._[1]
  switch (subcommand) {
    case 'delete':
      for (const dotted of Object.keys(flattened)) {
        unset(options, dotted)
      }
      await write(options)
      break

    case 'set':
    case 'write':
      merge(options, payload)
      await write(options)
      break

    case 'get':
    case 'read':
    default:
      for (const dotted of Object.keys(flattened)) {
        console.log(dotted, get(options, dotted))
      }
      break
  }
}
