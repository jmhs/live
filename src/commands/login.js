const {prompt} = require('inquirer')
const {read, write} = require('../options')
const {Client} = require('../auth0-api')
const Listr = require('listr')

module.exports.command = ['login']
module.exports.aliases = ['signin']
module.exports.desc = 'Authenticate an account'
module.exports.builder = {
  username: {
    type: 'string',
    describe: 'Account identifier'
  },
  password: {
    type: 'string',
    describe: 'Private secret key'
  }
}

module.exports.handler = async function handler (argv) {
  const questions = []
  if (argv.username === undefined) {
    questions.push({
      name: 'username',
      message: 'Username or email address:',
      default: (await read()).username
    })
  }
  if (argv.password === undefined) {
    questions.push({
      type: 'password',
      name: 'password',
      message: 'Password:'
    })
  }
  const answers = await prompt(questions)

  const username = argv.username || answers.username
  const password = argv.password || answers.password

  await new Listr([{
    title: 'Authenticating',
    task: async (context) => {
      const auth0 = new Client()
      const response = await auth0.getTokenByPassword(username, password)
      context.accessToken = response.access_token
    }
  }, {
    title: 'Saving token',
    task: async ({accessToken}) => {
      const options = await read()
      options.username = username
      options.access_token = accessToken
      await write(options)
    }
  }]).run().catch(() => {})
}
