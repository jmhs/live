const {prompt} = require('inquirer')
const {read, write} = require('../options')
const {Client} = require('../auth0-api')
const Listr = require('listr')

module.exports.command = ['signup']
module.exports.aliases = ['register']
module.exports.desc = 'Create an account'
module.exports.builder = {
  email: {
    type: 'string',
    describe: 'Email address'
  },
  username: {
    type: 'string',
    describe: 'Account identifier'
  },
  password: {
    type: 'string',
    describe: 'Private secret key'
  }
}

module.exports.handler = async function handler (argv) {
  const questions = []
  if (argv.email === undefined) {
    questions.push({
      name: 'email',
      message: 'Email address:'
    })
  }
  if (argv.username === undefined) {
    questions.push({
      name: 'username',
      message: 'Username:'
    })
  }
  if (argv.password === undefined) {
    questions.push({
      type: 'password',
      name: 'password',
      message: 'Password:'
    })
  }
  const answers = await prompt(questions)

  const email = argv.email || answers.email
  const username = argv.username || answers.username
  const password = argv.password || answers.password

  const auth0 = new Client()
  await new Listr([{
    title: 'Registering account',
    task: async () => {
      await auth0.signup(email, password, username)
    }
  }, {
    title: 'Creating new authentication token',
    task: async (context) => {
      const response = await auth0.getTokenByPassword(email, password)
      context.accessToken = response.access_token
    }
  }, {
    title: 'Saving credentials',
    task: async ({accessToken}) => {
      const options = await read()
      options.username = email
      options.access_token = accessToken
      await write(options)
    }
  }]).run().catch(() => {})
}
