const {join} = require('path')
const {promisify} = require('util')
const {readFile, writeFile} = require('fs')
const userHome = require('user-home')
const mkdirp = require('mkdirp')

const dirpath = join(userHome, `.${process.title}`)
const filepath = join(dirpath, 'options.json')

module.exports.read = async function read () {
  try {
    const raw = await promisify(readFile)(filepath, 'utf8')
    return JSON.parse(raw)
  } catch (error) {
    return {}
  }
}

module.exports.write = async function write (data) {
  const raw = JSON.stringify(data, null, '  ')
  await promisify(mkdirp)(dirpath)
  await promisify(writeFile)(filepath, raw, 'utf8')
}
