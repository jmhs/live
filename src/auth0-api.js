const fetch = require('node-fetch')

const clientId = 'WX1Kgy0NWL069OaiaCJLQE3OL0gqzq2e'
const domain = 'http2.au.auth0.com'
const database = 'Username-Password-Authentication'
const audience = 'https://http2.live/'
const scope = 'deploy'

async function inspectError (response) {
  let message
  try {
    const data = await response.json()
    message = data.error_description || data.description
    if (!message) throw Error
  } catch (error) {
    message = `${response.statusText} ${await response.text()}`
  }
  return new Error(message)
}

module.exports.Client = class Client {
  constructor () {
    this.clientId = clientId
    this.domain = domain
    this.database = database
    this.audience = audience
    this.scope = scope
  }

  async POST (endpoint, payload) {
    const url = `https://${this.domain}${endpoint}`
    const body = JSON.stringify(payload)
    const options = {
      method: 'POST',
      headers: {'content-type': 'application/json'},
      body
    }
    const response = await fetch(url, options)
    if (response.ok) return response.json()
    else throw await inspectError(response)
  }

  async getTokenByPassword (username, password, scope = this.scope) {
    return this.POST('/oauth/token', {
      connection: this.database,
      client_id: this.clientId,
      grant_type: 'password',
      audience: this.audience,
      scope,
      username,
      password
    })
  }

  async signup (email, password, username) {
    return this.POST('/dbconnections/signup', {
      client_id: this.clientId,
      connection: this.database,
      email,
      password,
      username
    })
  }
}
