# @http2/live 🐿

Command line interface for the Http2live service.

## Installation

```
npm install --global @http2/live
```

Note: Local installation, without the `--global` option, is also supported and useful in [`npm scripts`](https://docs.npmjs.com/misc/scripts).

## Usage

```
$ http2live signup
? Email address: johnw@example.net
? Username: babayaga
? Password: ***************
 ✔ Registering account
 ✔ Creating new authentication token
 ✔ Saving credentials

$ http2live deploy
 ↓ Validating configuration [skipped]
   → Using default options
 ✔ Checking credentials
 ✔ Indexing files
 ✔ Uploading
```

## Commands

### `signup`

Alias: `register`

Register a new account.

#### `--username`

Email or username for the account.

Optional. If supplied the prompt for this value will be skipped.

#### `--password`

Secret password key for the account.

Optional. If supplied the prompt for this value will be skipped.

### `login`

Alias: `signin`

Authenticates the account credentials. Only one account is signed-in at a time.

#### `--username`

Email or username for the account.

Optional. If supplied the prompt for this value will be skipped.

#### `--password`

Secret password key for the account.

Optional. If supplied the prompt for this value will be skipped.

### `logout`

Alias: `signout`

### `whoami`

Display currently authenticated username or password.

### `deploy`

Aliases: `publish`, `serve`, `sync`, `upload`

Transfer static files and configuration options to the service.

#### `--config`

Path to a valid [`@http2/configuration`](https://gitlab.com/http2/configuration) options file. Only the first host is used.

#### `--root`

Path to a directory containing static files to upload.

### `config [get|set|delete]`

Aliases: `configure`, `configuration`, `setting`, `settings`, `option`, `options`

Edit the user settings.

Specify the setting name and value as `--my.setting.name "my setting value"`. Dot-notation is used for nested property names.

Multiple settings may be configured in sequence. `--foo=bar --lol=true`.

Settings can be removed by specifying the `delete` subdommand.

#### Example Configuration

```js
{
  username: '...', // See: login
  access_token: '...' // See: login,

  api: {
    domain: 'api.http2.live',
    port: 8888
  }
}
```

## References

- [@http2/api](https://gitlab.com/http2/api)
- [@http2/server](https://gitlab.com/http2/server)
- [@http2/configuration](https://gitlab.com/http2/configuration)
