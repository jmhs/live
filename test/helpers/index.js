import {resolve} from 'path'
import {rename} from 'fs'
import {promisify} from 'util'
import userHome from 'user-home'
import readPkgUp from 'read-pkg-up'
import rimraf from 'rimraf'
import mkdirp from 'mkdirp'
import cpFile from 'cp-file'

async function mv (from, to) {
  try {
    await promisify(rename)(from, to)
  } catch (error) {
    if (error.code !== 'ENOENT') {
      throw error
    }
  }
}

async function getDirectory () {
  const {pkg} = await readPkgUp({cwd: __dirname})
  const [name] = Object.keys(pkg.bin)
  const directory = resolve(userHome, `.${name}`)
  return directory
}

async function backup () {
  const directory = await getDirectory()
  const from = directory
  const to = `${directory}.backup`
  return mv(from, to)
}

export async function restore () {
  const directory = await getDirectory()
  const from = `${directory}.backup`
  const to = directory
  await promisify(rimraf)(to)
  try {
    return mv(from, to)
  } catch (error) {
    if (error.code !== 'ENOENT') {
      throw error
    }
  }
}

export async function prepare (scenario) {
  await backup()
  if (scenario !== undefined) {
    const directory = await getDirectory()
    await promisify(mkdirp)(directory)
    const source = resolve(process.cwd(), 'test/fixtures', scenario)
    const destination = resolve(directory, 'options.json')
    await cpFile(source, destination, {overwrite: false})
  }
}
