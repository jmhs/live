import {resolve} from 'path'
import {spawn, spawnSync} from 'child_process'

const bin = resolve(process.cwd(), 'bin.js')

function runner (method, args, opts) {
  if (typeof args === 'string') {
    args = args.split(' ')
  }
  return method(process.argv[0], [bin, ...args], Object.assign({
    cwd: process.cwd(),
    timeout: 10000,
    killSignal: 'SIGKILL',
    encoding: 'utf8'
  }, opts))
}

export function runSync (args, opts = {}) {
  return runner(spawnSync, args, opts)
}

export function run (args, opts = {}) {
  return runner(spawn, args, opts)
}
