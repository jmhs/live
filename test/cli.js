import test from 'ava'
import path from 'path'
import {prepare, restore} from './helpers'
import {run, runSync} from './helpers/runner'
import {createServer} from 'http'
import jsonfile from 'jsonfile'
import Busboy from 'busboy'
import {promisify} from 'util'
import read from 'read-all-stream'

test.before(async (t) => restore())
test.afterEach.always(async (t) => restore())

test.serial('report logged in user', async (t) => {
  await prepare('logged_in.json')
  const done = runSync('whoami')
  t.is(done.stdout, 'johnw@example.net\n')
})

test.serial('report nothing when logged out', async (t) => {
  await prepare('logged_out.json')
  const done = runSync('whoami')
  t.is(done.stdout, '')
})

test.serial('config set/get', async (t) => {
  await prepare()
  runSync('config set --foo.bar 123')
  const done = runSync('config --foo.bar')
  t.is(done.stdout, 'foo.bar 123\n')
})

test.serial('config delete/get', async (t) => {
  await prepare('config_delete.json')
  runSync('config delete --foo.bar')
  const done = runSync('config --foo.bar')
  t.is(done.stdout, 'foo.bar undefined\n')
})

test.serial('logout', async (t) => {
  await prepare('logged_in.json')
  runSync('logout')
  const done = runSync('config --username --access_token')
  t.is(done.stdout, 'username undefined\naccess_token undefined\n')
})

test.serial('run from any directory', async (t) => {
  await prepare('logged_in.json')
  const root = path.resolve(path.sep)
  const done = runSync('whoami', {cwd: root})
  t.is(done.stdout, 'johnw@example.net\n')
})

test.serial('deploy', (t) => new Promise(async (resolve) => {
  t.plan(9)
  const server = createServer(async (request, response) => {
    const fixture = path.resolve(process.cwd(), 'test/fixtures/deploy.json')
    const {access_token: expectedBearer} = jsonfile.readFileSync(fixture)
    t.is(request.headers.authorization, `Bearer ${expectedBearer}`)
    const busboy = new Busboy({headers: request.headers, preservePath: true})
    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
      t.is(fieldname, 'directory')
      switch (filename) {
        case 'stuff/foo.bar':
          t.is(mimetype, 'application/octet-stream')
          break
        case 'index.html':
          t.is(mimetype, 'text/html')
          break
        default:
          t.fail('Wrong filename')
      }
      file.resume()
    })
    busboy.on('field', (fieldname, val, fieldnameTruncated, valTruncated) => {
      t.is(fieldname, 'options')
      t.is(val, '{}')
    })
    busboy.on('finish', () => {
      response.writeHead(200)
      response.end(JSON.stringify({domain: 'foo.bar'}))
      server.close()
    })
    request.pipe(busboy)
  })
  server.listen(0, async () => {
    await prepare('deploy.json') // JWT signing key: "secret"
    const port = server.address().port
    runSync(`config set --api.origin http://localhost:${port}`)
    const root = 'test/fixtures/deploy/root'
    const config = 'test/fixtures/deploy/serverconfig.js'
    const child = run(`deploy --root ${root} --config ${config}`)
    const output = await promisify(read)(child.stdout)
    t.is(true, output.includes('Deployment successful'))
    t.is(true, output.includes('https://foo.bar/'))
    resolve()
  })
}))
